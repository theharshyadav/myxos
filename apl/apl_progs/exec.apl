integer main()
{
	integer pid;
	integer retVal;
	print("Before Exec");
	pid = Fork();
	print("After Exec");
	if (pid == -2) then
		retVal = Exec("even.xsm");
	else
		retVal = Exec("odd.xsm");
	endif;
	return 0;
}
