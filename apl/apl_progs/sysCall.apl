decl
	integer status, fileDescriptor, choice, flag, i, newLseek;
	string filename, word, wordRead;
enddecl

integer main(){
	i = 1;
	while(i == 1) do
		flag = -1;
		print("1. Create");
		print("2. Open");
		print("3. Close");
		print("4. Delete");
		print("5. Write");
		print("6. Lseek");
		print("7. Read");
		print("-1. Exit");
		read (choice);
		print("**********");	
		if (choice == -1) then
			break;
		endif;
		if (choice == 1) then				// Create
			print("Enter the filename");
			read (filename);
			status = Create(filename);
			flag = 1;
		endif;
		if (choice == 2 ) then				// Open
			print("Enter the filename");
			read (filename);
			fileDescriptor = Open(filename);
			print("FD:");
			print(fileDescriptor);
			flag = 1;
		endif;
		if (choice == 3 ) then				// Close
			print("Enter FD");
			read (fileDescriptor);
			status = Close (fileDescriptor);
			flag = 1;
		endif;
		if (choice == 4 ) then				// Delete
			print("Enter filename");
			read (filename);
			status = Delete(filename);
			flag = 1;
		endif;
		if (choice == 5) then				// Write
			print ("Enter FD");
			read (fileDescriptor);
			print ("Enter word");
			read (word);
			status = Write(fileDescriptor, word);
			flag = 1;
		endif;
		if (choice == 6) then				// Lseek
			print("Enter FD");
			read (fileDescriptor);
			print("Enter newLseek");
			read (newLseek);
			status = Seek (fileDescriptor, newLseek);
			flag = 1;
		endif;
		if (choice == 7) then				// Read
			print("Enter FD");
			read (fileDescriptor);
			status = Read (fileDescriptor, wordRead);
			print(word);
			flag = 1;
		endif;


		if (flag == -1) then				// Rest Choices
			print("Incorrect!!");
		endif;
		print("**********");
	endwhile;
	print("**********");
	return 0;
}
